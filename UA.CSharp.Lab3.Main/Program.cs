﻿using System;
using System.Collections.Generic;

namespace UA.CSharp.Lab3.Main
{
    public static class Program
    {
        private static void Main()
        {
            var random = new Random();

            var students = GetStudents();

            var libraryItems = GetLibraryItems();

            var transaction = new Print
            {
                Student = students[random.Next(students.Count)],
                LibraryItem = libraryItems[random.Next(libraryItems.Count)]
            };

            transaction.CheckOut();

            Console.WriteLine(transaction.Student.FirstLastName + " checked out " + transaction.LibraryItem.Title + ".");
            Console.WriteLine(transaction.Student.FirstLastName + " has " + transaction.Student.CheckedOutItems.Count + " items checked out.");

            transaction.CheckIn();

            Console.WriteLine(transaction.Student.FirstLastName + " has now returned " + transaction.LibraryItem.Title + ".");

            var transactions = new List<Transaction> { transaction };

            Console.ReadKey();
        }

        private static List<Student> GetStudents()
        {
            return new List<Student>
            {
                new Undergraduate
                {
                    ID = 1,
                    FirstName = "Bailey",
                    MiddleInitial = "A",
                    LastName = "Parrish",
                    Status = Status.Junior,
                    Major = "BS Computer Information Systems"
                },
                new Undergraduate
                {
                    ID = 2,
                    FirstName = "Riley",
                    MiddleInitial = "J",
                    LastName = "Miller",
                    Status = Status.Senior,
                    Major = "BSN"
                },
                new Undergraduate
                {
                    ID = 3,
                    FirstName = "Micheal",
                    MiddleInitial = "B",
                    LastName = "Sweazy",
                    Status = Status.Freshman,
                    Major = "ADN"
                },
                new Graduate
                {
                    ID = 4,
                    FirstName = "Abby",
                    LastName = "Smith",
                    GraduateProgram = "MSN Psychology"
                },
                new HonorUndergraduate
                {
                    ID = 5,
                    FirstName = "Blake",
                    LastName = "Stoller",
                    Status = Status.Freshman,
                    Major = "BS Computer Engineering",
                    HasHonorsHousing = false
                }
            };
        }

        private static List<LibraryItem> GetLibraryItems()
        {
            return new List<LibraryItem>
            {
                new Book
                {
                    ID = 1,
                    ISBN = "0141439602",
                    Title = "A Tale of Two Cities",
                    Author = "Charles Dickens",
                    YearPublished = 1859,
                    PageLength = 489,
                    Publisher = "Penguin Books"
                },
                new Book
                {
                    ID = 2,
                    ISBN = "0618346252",
                    Title = "The Fellowship of the Ring",
                    Author = "J.R.R. Tolkien",
                    YearPublished = 1954,
                    PageLength = 398,
                    Publisher = "Houghton Mifflin Harcourt"
                },
                new Book
                {
                    ID = 3,
                    ISBN = "0006754023",
                    Title = "The Hobbit",
                    Author = "J.R.R. Tolkien",
                    YearPublished = 1937,
                    PageLength = 365,
                    Publisher = "HarperCollins Children's Books"
                },
                new Book
                {
                    ID = 4,
                    ISBN = "0316769177",
                    Title = "The Catcher in the Rye",
                    Author = "J.D. Salinger",
                    YearPublished = 1951,
                    PageLength = 277,
                    Publisher = "Back Bay Books"
                },
                new AudioBook
                {
                    ID = 5,
                    ISBN = "0545044251",
                    Title = "Harry Potter Series Box Set",
                    Author = "J.K. Rowling",
                    YearPublished = 2007,
                    Publisher = "Arthur A. Levine Books",
                    MinutesLong = 7496
                },
                new AudioBook
                {
                    ID = 6,
                    ISBN = "030788743X",
                    Title = "Ready Player One",
                    Author = "Ernest Cline",
                    YearPublished = 2011,
                    PageLength = 374,
                    Publisher = "Crown Publishers",
                    MinutesLong = 940
                },
                new EBook
                {
                    ID = 7,
                    ISBN = "9780679783268",
                    Title = "Pride and Prejudice",
                    Author = "Jane Austin",
                    YearPublished = 1813,
                    PageLength = 279,
                    Publisher = "Modern Library"
                },
                new EBook
                {
                    ID = 8,
                    ISBN = "0393970124",
                    Title = "Dracula",
                    Author = "Bram Stoker",
                    YearPublished = 1897,
                    PageLength = 488,
                    Publisher = "Norton"
                },
                new Magazine
                {
                    ID = 9,
                    Title = "National Geographic",
                    Publisher = "National Geographic Society"
                },
                new Magazine
                {
                    ID = 10,
                    Title = "Sports Illustrated",
                    Publisher = "Meredith Corporation"
                }
            };
        }
    }
}
