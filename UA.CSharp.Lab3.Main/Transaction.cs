﻿namespace UA.CSharp.Lab3.Main
{
    public abstract class Transaction
    {
        public Student Student { get; set; }

        public LibraryItem LibraryItem { get; set; }

        public void CheckIn()
        {
            Student.CheckedOutItems.Remove(LibraryItem);
            LibraryItem.CheckedOut = false;
        }

        public void CheckOut()
        {
            Student.CheckedOutItems.Add(LibraryItem);
            LibraryItem.CheckedOut = true;
        }
    }

    public class Print : Transaction { }

    public class DigitalMedia : Transaction { }
}
