﻿using System;

namespace UA.CSharp.Lab3.Main
{
    public abstract class LibraryItem
    {
        public int ID { get; set; }

        public string ISBN { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public int YearPublished { get; set; }

        public int? PageLength { get; set; }

        public bool CheckedOut { get; set; }

        public string Publisher { get; set; }
    }

    public class Book : LibraryItem { }

    public class AudioBook : LibraryItem
    {
        public int MinutesLong { get; set; }
    }

    public class EBook : LibraryItem { }

    public class Magazine : LibraryItem
    {
        public int IssueNum { get; set; }
    }
}
