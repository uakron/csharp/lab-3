﻿using System;
using System.Collections.Generic;

namespace UA.CSharp.Lab3.Main
{
    public enum Status { Freshman, Sophomore, Junior, Senior }

    public abstract class Student
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string MiddleInitial { get; set; }

        public string LastName { get; set; }

        public string FirstLastName => FirstName + " " + LastName;

        public Status? Status { get; set; }

        public List<LibraryItem> CheckedOutItems = new List<LibraryItem>();
    }

    public class Undergraduate : Student
    {
        public readonly int BookLimit = 6;

        public readonly int WeekLimit = 2;

        public string Major { get; set; }
    }

    public class HonorUndergraduate : Undergraduate
    {
        new public readonly int BookLimit = 8;

        new public readonly int WeekLimit = 3;

        public bool HasHonorsHousing { get; set; }
    }

    public class Graduate : Student
    {
        public readonly int BookLimit = 10;

        public readonly int WeekLimit = 4;

        public string GraduateProgram { get; set; }
    }
}
